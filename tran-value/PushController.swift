//
//  PushController.swift
//  tran-value
//
//  Created by Howard on 2021/7/19.
//

import UIKit

protocol Protocol1: NSObjectProtocol {
    func retValue(text: String)
}

class PushController: UIViewController {
    
    private var label: UILabel!
    
    private var protocol1: Protocol1?
    
    private let fullSize = UIScreen.main.bounds.size
    
    init() {
        super.init(nibName: nil, bundle: nil)
        
        label = UILabel(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        
        label.center = CGPoint(x: fullSize.width*0.5, y: fullSize.height*0.5)
        
        label.backgroundColor = .black
        
        label.textColor = .white
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        view.addSubview(label)
        
        let back = UIButton(frame: CGRect(x: 0, y: 0, width: 160, height: 100))
        
        back.center = CGPoint(x: fullSize.width*0.5, y: fullSize.height*0.5+210)
        
        back.backgroundColor = .blue
        
        back.setTitle("回退", for: .normal)
        
        back.addTarget(self, action: #selector(pop), for: .touchUpInside)
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .stop, target: self, action: #selector(pop))
        
        view.addSubview(back)
    }
    
    public func setLabelText(text: String) {
        label.text = text
    }
    
    public func setProtocol(protocol1: Protocol1) {
        self.protocol1 = protocol1
    }
    
    @objc func pop() {
        // 协议传值
        // 实质保存了上一级对象再赋值
        protocol1?.retValue(text: "从Push回来")
        
        self.navigationController?.popViewController(animated: true)
    }
}
