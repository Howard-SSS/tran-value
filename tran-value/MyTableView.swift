//
//  MyTableView.swift
//  MyTableView
//
//  Created by Howard on 2021/7/18.
//

import UIKit

struct TableGound<T> {
    var name: String
    var arr: [T]
}
class MyTableView: UITableView {
    
    private var data: [TableGound<Any>] = []

    public override init(frame: CGRect, style: UITableView.Style) {
        
        super.init(frame: frame, style: style)
        
        super.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        
        // 数据初始化
        data = [
            TableGound(name: "Gound1", arr: ["Gound1.no1","Gound1.no2","Gound1.no3","Gound1.no4"]),
            TableGound(name: "Gound2", arr: ["Gound2.no1","Gound2.no2","Gound2.no3","Gound2.no4"]),
            TableGound(name: "Gound3", arr: ["Gound3.no1","Gound3.no2","Gound3.no3","Gound3.no4"])
        ]
        
        delegate = self
        
        dataSource = self
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setData(data: [TableGound<Any>]) {
        self.data = data
    }
    
    func addData(_ value: Any, section: Int, row: Int = 0) {
        data[section].arr.insert(value, at: row)
    }
}

extension MyTableView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // 消除选中状态
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
extension MyTableView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let sourceSection = sourceIndexPath.section
        let destinationSection = destinationIndexPath.section
        let sourceValue = data[sourceSection].arr[sourceIndexPath.row]
        if sourceSection == destinationSection && sourceIndexPath.row == destinationIndexPath.row {
        }else{
            data[sourceSection].arr.remove(at: sourceIndexPath.row)
            data[destinationSection].arr.insert(sourceValue, at: destinationIndexPath.row)
        }
    }
    /*
     编辑行
     */
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // 删除数据源一行
            self.data[indexPath.section].arr.remove(at: indexPath.row)
            // 删除界面一行
            tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.fade)
        }
    }
    
    /*
     section 标题
     */
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return data[section].name
    }
    
    /*
     是否开启编辑行（增/删）
     */
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    /*
     返回每组多少个单元
     */
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data[section].arr.count
    }
    
    /*
     定制每个表格的样式并返回
     */
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.accessoryType = .none
        // 显示内容
        if let myLabel = cell.textLabel {
            myLabel.text = "\(data[indexPath.section].arr[indexPath.row])"
        }
        // 选中背景变色
        cell.selectedBackgroundView = UIView()
        cell.selectedBackgroundView?.backgroundColor = .green
        // 选中显示颜色
        cell.textLabel?.highlightedTextColor = .red
        return cell
    }
    
    /*
     有几组
     */
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
}
