//
//  ViewController.swift
//  tran-value
//
//  Created by Howard on 2021/7/19.
//

import UIKit

class ViewController: UIViewController, Protocol1 {

    private let fullScreenSize = UIScreen.main.bounds.size
    
    var textField: UITextField!
    init() {
        super.init(nibName: nil, bundle: nil)
        
        textField = UITextField(frame: CGRect(x: 0, y: 0, width: 300, height: 120))
        
        textField.center = CGPoint(x: fullScreenSize.width*0.5, y: 100)
        
        textField.placeholder = "請輸入文字"
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(usePush))
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(usePresent))
        
        view.addSubview(textField)
    }

    func retValue(text: String) {
        textField.text = text
    }
    
    // push 跳转
    @objc func usePush() {
        let controller = PushController()
        // 通过属性直接传值
        controller.setLabelText(text: textField.text ?? "nil")
        
        controller.setProtocol(protocol1: self)
        
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    // present 跳转
    @objc func usePresent() {
        let controller = PresentController()

        let temp:String = textField.text != "" ? textField.text! : "10"
        controller.setEnd(end: Int(temp)!)
        
        self.present(controller, animated: true, completion: nil)
    }
}

