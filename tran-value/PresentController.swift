//
//  File.swift
//  NyTest1
//
//  Created by Howard on 2021/7/14.
//

import UIKit

class PresentController: UIViewController {
    
    private var myTableView: MyTableView!
    
    private let fullScreenSize = UIScreen.main.bounds.size
    
    private var end: Int?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        myTableView = MyTableView(frame: CGRect(x: 0, y: 0, width: fullScreenSize.width, height: fullScreenSize.height), style: .grouped)
        myTableView.setEditing(false, animated: true)
        do {
            try myTableView.setData(data: [fibonaqi(begin: 1, end: end!)])
        }catch {
            print("function use error")
        }
        self.view.addSubview(myTableView)
    }
    
    func setEnd(end: Int) {
        self.end = end
    }
    /*
     斐波那契
     */
    func fibonaqi(begin: Int, end: Int) throws -> TableGound<Any> {
        if begin > end {
            throw MyError.USEERROR
        }
        var tableGound = TableGound<Any>(name: "fibonaqi", arr: [])
        for i in begin...end {
            let s5 = sqrt(5)
            let q = pow((1.0 + s5) / 2, Double(i)) - pow((1.0 - s5) / 2, Double(i))
            let res: Int = Int(round(q / s5))
            tableGound.arr.append(res)
        }
        return tableGound
    }
    
    @objc func addBtnAction() {
        myTableView.addData("new value", section: 0)
        
        myTableView.beginUpdates()
        myTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
        myTableView.endUpdates()
    }
}

enum MyError: Error {
    case USEERROR
}
